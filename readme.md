# Match API
A Spring boot service for querying data of a match.

## Usage
The service imports match data on startup, from `config/match_timeline.json`. Points per goal and yellow card can be configured in `config/application.properties`. It is started by running `docker-compose up -d` in the root of the repository. On server startup, the database will get configured automatically, and data will be imported.
The service will be available at `localhost`, the database will be at `localhost:3306` and the optional administration tool for the database will be at `localhost:8080`.

### Endpoints
 * `/match/{id}` - Returns all the data for the match as provided in `match_timeline.json`
 * `/match/{id}/event/{eventType}` - Returns all events for a match of a provided `eventType`
 * `/match/{id}/event/tominute/{minute}` - Returns all the events of a match, up to and including `minute`
 * `/match/{id}/player/{playerId}` - Calculates the points for a given player, and returns them, as well as all the events the player was part of.
 * `/match/{id}/winnerscorers` - Returns all the players who have scored for the winning team. If the match was a draw, returns all the scorers on both teams.