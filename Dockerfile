FROM maven:3.6.3-amazoncorretto-11 as maven

COPY target/*.jar app.jar

ENTRYPOINT [ "sh", "-c", "java ${JAVA_OPTS} -jar /app.jar --spring.config.location=file:/config/application.properties" ]