package com.lukapg.match;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lukapg.match.model.*;
import org.junit.jupiter.api.Test;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MatchDeserializerTests {
    private String _singleMatchTimelineLocation = "classpath:single_match.json";

    @Test
    void DeserializesSingleMatch_IntoSingleLengthArrayList() {
        // Act
        ArrayList<Match> matches = deserialize(_singleMatchTimelineLocation).getMatches();

        // Assert
        assertEquals(1, matches.size());
    }

    @Test
    void ValidMatchObject_WithoutEvents(){
        // Arrange
        Result exResult = new Result(3, 2);
        Team home = new Team("Aston Villa", 7306474);
        Team away = new Team("Burton", 7333812);
        ArrayList<Event> events = fillSingleMatchEvents();
        Match expected = new Match(11868682, exResult, home, away, events);

        // Act
        Match deserializeResult = deserialize(_singleMatchTimelineLocation).getMatches().get(0);

        // Assert
        assertEquals(expected, deserializeResult);
    }


    // =======================Helpers==========================

    private ArrayList<Event> fillSingleMatchEvents() {
        ArrayList<Event> result = new ArrayList<>();
        result.add(new Event("Corner kick", "corner", 4, Side.home, new Player("", -1, 0, Side.home)));
        result.add(new Event("Yellow card", "card", 69, Side.home, new Player("Grealish, Jack", 189061, 0, Side.home)));
        result.add(new Event("Goal", "goal", 71, Side.away, new Player("Elmohamady, Ahmed", 32787, 0, Side.away)));
        result.add(new Event("Yellow card", "card", 73, Side.away, new Player("Elmohamady, Ahmed", 32787, 0, Side.away)));
        result.add(new Event("Substitution", "substitution", 84, Side.home, new Player("Hourihane, Conor", 100574, 0, Side.home)));
        result.add(new Event("Goal", "goal", 88, Side.home, new Player("Grealish, Jack", 189061, 0, Side.home)));
        result.add(new Event("Injury time", "injurytimeshown", 90, Side.both));
        result.add(new Event("Goal", "goal", 90, Side.away, new Player("Boyce, Liam", 77338, 0, Side.away)));

        return result;
    }

    private MatchTimeline deserialize(String timelineLocation) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(ResourceUtils.getFile(timelineLocation), MatchTimeline.class);
        } catch (IOException ioe) {
            System.err.println("Error while reading match file\n" + ioe.getMessage());
            return new MatchTimeline();
        }
    }
}
