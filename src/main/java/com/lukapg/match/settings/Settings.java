package com.lukapg.match.settings;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("app")
public class Settings {
    private int goalPoints;
    private int cardPoints;
    private String timeLinePath;

    public int getGoalPoints() {
        return goalPoints;
    }

    public int getCardPoints() {
        return cardPoints;
    }

    public void setCardPoints(int cardPoints) {
        this.cardPoints = cardPoints;
    }

    public void setGoalPoints(int goalPoints) {
        this.goalPoints = goalPoints;
    }

    public String getTimeLinePath() {
        return timeLinePath;
    }

    public void setTimeLinePath(String timeLinePath) {
        this.timeLinePath = timeLinePath;
    }
}
