package com.lukapg.match;

import com.lukapg.match.domain.MatchDomainService;
import com.lukapg.match.model.Event;
import com.lukapg.match.model.Match;
import com.lukapg.match.model.Player;
import com.lukapg.match.model.PlayerDataResponse;
import com.lukapg.match.repository.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping(path = "match/")
public class MatchController {
    @Autowired private MatchRepository matchRepository;
    @Autowired private MatchDomainService _domainService;

    /**
     * Retrieves all the data of a given match that we store
     * @param matchId ID of the match to query
     * @return All the data for the given match
     * @throws ChangeSetPersister.NotFoundException
     */
    @RequestMapping(path="{id}")
    public Match getMatch(@PathVariable("id") int matchId) throws ChangeSetPersister.NotFoundException {
        return matchRepository.findById(matchId).orElseThrow(ChangeSetPersister.NotFoundException::new);
    }

    /**
     * Retrieves all events of a match with a given type
     * @param id ID of the match to query
     * @param eventType Type of the event
     * @return List of events
     * @throws ChangeSetPersister.NotFoundException
     */
    @RequestMapping(path = "{id}/event/{eventType}")
    public ArrayList<Event> getEventsWithType(@PathVariable("id") int id, @PathVariable("eventType") String eventType) throws ChangeSetPersister.NotFoundException {
        return _domainService.getEvents(id, eventType);
    }

    /**
     * Retrieves all events of a match, up to and including the provided time
     * @param id ID of the match to query
     * @param minute Last minute
     * @return List of events
     * @throws ChangeSetPersister.NotFoundException
     */
    @RequestMapping(path = "{id}/event/tominute/{minute}")
    public ArrayList<Event> getEventsUntil(@PathVariable("id") int id, @PathVariable("minute") int minute) throws ChangeSetPersister.NotFoundException {
        return _domainService.getEventsUntil(id, minute);
    }

    /**
     * Calculates points for a player in the match, and returns them, together with all events the player participated in
     * @param matchId ID of the match to query
     * @param playerId ID of the player to query
     * @return Object with points and a list of player's events
     * @throws ChangeSetPersister.NotFoundException
     */
    @RequestMapping(path = "{id}/player/{playerId}")
    public PlayerDataResponse getPlayerData(@PathVariable("id") int matchId, @PathVariable("playerId") int playerId) throws ChangeSetPersister.NotFoundException {
        return _domainService.getPlayerInfo(matchId, playerId);
    }

    /**
     * Calculates and returns all the scoring players for the winning team, or both teams if the match was a draw
     * @param matchId ID of the match to query
     * @return List of players with calculated points
     * @throws ChangeSetPersister.NotFoundException
     */
    @RequestMapping(path = "{id}/winnerscorers")
    public ArrayList<Player> getScorers(@PathVariable("id") int matchId) throws ChangeSetPersister.NotFoundException {
        return _domainService.getScorers(matchId);
    }


}
