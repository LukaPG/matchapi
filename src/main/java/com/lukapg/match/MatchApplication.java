package com.lukapg.match;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lukapg.match.model.MatchTimeline;
import com.lukapg.match.repository.MatchRepository;
import com.lukapg.match.settings.Settings;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassRelativeResourceLoader;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.repository.core.EntityInformation;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import java.io.IOException;

@SpringBootApplication
public class MatchApplication {
    public static void main(String[] args) {
        var context = SpringApplication.run(MatchApplication.class, args);
    }
}
