package com.lukapg.match.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Player {
    @Id private int id;
    private String name;
    private int points;
    @Enumerated(EnumType.STRING) private Side team;

    public Player() {}
    public Player(String name, int id, int points, Side team) {
        this.name = name;
        this.id = id;
        this.points = points;
        this.team = team;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void addPoints(int points) {
        this.points += points;
    }

    public Side getTeam() {
        return team;
    }

    //Generated by IntelliJ...
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        if (id != player.id) return false;
        if (points != player.points) return false;
        if (!Objects.equals(name, player.name)) return false;
        return team == player.team;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + id;
        result = 31 * result + points;
        result = 31 * result + (team != null ? team.hashCode() : 0);
        return result;
    }
}
