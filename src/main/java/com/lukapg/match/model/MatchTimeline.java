package com.lukapg.match.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lukapg.match.utils.MatchDeserializer;

import java.util.ArrayList;


/**
 * Wrapper class for easier deserialization from json file
 */
@JsonDeserialize(using = MatchDeserializer.class)
public class MatchTimeline {
    private ArrayList<Match> matches;

    public MatchTimeline() {
        this(new ArrayList<>());
    }

    public MatchTimeline(ArrayList<Match> matches) {
        this.matches = matches;
    }

    public ArrayList<Match> getMatches() {
        return matches;
    }
}
