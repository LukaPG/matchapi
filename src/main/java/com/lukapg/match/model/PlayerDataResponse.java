package com.lukapg.match.model;

import java.util.List;

public class PlayerDataResponse {
    private int playerPoints;
    private List<Event> events;

    public PlayerDataResponse(int playerPoints, List<Event> events) {
        this.playerPoints = playerPoints;
        this.events = events;
    }


    public int getPlayerPoints() {
        return playerPoints;
    }

    public List<Event> getEvents() {
        return events;
    }
}
