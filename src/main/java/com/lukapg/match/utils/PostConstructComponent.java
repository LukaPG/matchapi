package com.lukapg.match.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lukapg.match.MatchApplication;
import com.lukapg.match.model.MatchTimeline;
import com.lukapg.match.repository.MatchRepository;
import com.lukapg.match.repository.PlayerRepository;
import com.lukapg.match.repository.TeamRepository;
import com.lukapg.match.settings.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassRelativeResourceLoader;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class PostConstructComponent {
    @Autowired private Settings settings;
    @Autowired private MatchRepository matchRepository;
    @Autowired private TeamRepository teamRepository;
    @Autowired private PlayerRepository playerRepository;

    @PostConstruct
    public void init() {

        // Set location to external file if its in the configuration
        var location = settings.getTimeLinePath() != null
                ? "file:" + settings.getTimeLinePath()
                : "classpath:static/match_timeline.json";

        MatchTimeline timeline = deserializeMatches(location);
        
        for (var match : timeline.getMatches()) {
            teamRepository.save(match.getAwayTeam());
            teamRepository.save(match.getHomeTeam());
            for (var event : match.getEvents()) {
                if (event.getPlayer() != null && !playerRepository.existsById(event.getPlayer().getId())){
                    playerRepository.save(event.getPlayer());
                }
                event.setMatch(match);
            }

            matchRepository.save(match);


        }
    }

    private MatchTimeline deserializeMatches(String timelineLocation) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            var loader = new ClassRelativeResourceLoader(MatchApplication.class);

            return mapper.readValue(loader.getResource(timelineLocation).getInputStream(), MatchTimeline.class);
        } catch (IOException ioe) {
            System.err.println("Error while reading match file\n" + ioe.getMessage());
            return new MatchTimeline();
        }
    }
}
