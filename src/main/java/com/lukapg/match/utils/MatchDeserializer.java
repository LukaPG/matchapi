package com.lukapg.match.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.lukapg.match.model.*;

import java.io.IOException;
import java.util.ArrayList;

public class MatchDeserializer extends StdDeserializer<MatchTimeline> {

    public MatchDeserializer() {this(null);}
    public MatchDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public MatchTimeline deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException, JsonProcessingException {
        ArrayList<Match> result = new ArrayList<>();

        JsonNode json = jsonParser.getCodec().readTree(jsonParser);
        json.get("doc").elements().forEachRemaining(doc -> {
            var matchData = doc.get("data").get("match");
            int matchId = matchData.get("_id").asInt();

            // Find scores and parse into result
            int homeScore = matchData.get("result").get("home").asInt();
            int awayScore = matchData.get("result").get("away").asInt();
            Result matchResult = new Result(homeScore, awayScore);

            // Find playing teams metadata
            var homeTeam = matchData.get("teams").get("home");
            var awayTeam = matchData.get("teams").get("away");
            Team home = new Team(homeTeam.get("name").asText(), homeTeam.get("_id").asInt());
            Team away = new Team(awayTeam.get("name").asText(), awayTeam.get("_id").asInt());

            // Parse events into an array list
            ArrayList<Event> events = new ArrayList<>();
            var eventsData = doc.get("data").get("events");
            eventsData.elements().forEachRemaining(eventNode -> {
                String name = eventNode.get("name").asText();
                String type = eventNode.get("type").asText();
                int time = eventNode.get("time").asInt();

                // Convert team side into a strongly typed enum
                String teamSide = eventNode.get("team").asText("both");
                if (teamSide.isEmpty()) teamSide = "both";
                Side side = Side.valueOf(teamSide);
                String playerFieldName = type.equals("goal") ? "scorer" : "player";
                if (eventNode.has(playerFieldName)) {
                    JsonNode playerNode = eventNode.get(playerFieldName);
                    String playerName = playerNode.get("name").asText();
                    int playerId = playerNode.has("_id") ? playerNode.get("_id").asInt() : -1; // -1 for invalid players
                    int points = 0;
                    Player player = new Player(playerName, playerId, points, side);

                    events.add(new Event(name, type, time, side, player));
                } else {
                    events.add(new Event(name, type, time, side));
                }
            });

            result.add(new Match(matchId, matchResult, home, away, events));
        });

        return new MatchTimeline(result);
    }
}
