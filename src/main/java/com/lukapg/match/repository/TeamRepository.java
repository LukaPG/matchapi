package com.lukapg.match.repository;

import com.lukapg.match.model.Team;
import org.springframework.data.repository.CrudRepository;

public interface TeamRepository extends CrudRepository<Team, Integer> {
}
