package com.lukapg.match.repository;

import com.lukapg.match.model.Match;
import org.springframework.data.repository.CrudRepository;

public interface MatchRepository extends CrudRepository<Match, Integer> {

}
