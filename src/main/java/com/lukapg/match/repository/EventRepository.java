package com.lukapg.match.repository;

import com.lukapg.match.model.Event;
import org.springframework.data.repository.CrudRepository;

public interface EventRepository extends CrudRepository<Event, Integer> {
}
