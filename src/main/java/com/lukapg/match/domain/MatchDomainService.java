package com.lukapg.match.domain;

import com.lukapg.match.model.*;
import com.lukapg.match.repository.MatchRepository;
import com.lukapg.match.repository.PlayerRepository;
import com.lukapg.match.settings.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MatchDomainService {
    @Autowired private MatchRepository matchRepository;
    @Autowired private PlayerRepository playerRepository;
    @Autowired private Settings _settings;

    /**
     * Gets all events of a match with given event type
     * @param matchId IID of the match to query
     * @param eventType Type of the event to filter by
     * @return Filtered collection of Events with their type being eventType
     */
    public ArrayList<Event> getEvents(int matchId, String eventType) throws ChangeSetPersister.NotFoundException {
        var match = matchRepository.findById(matchId).orElseThrow(ChangeSetPersister.NotFoundException::new);

        return (ArrayList<Event>)
            match.getEvents()
                .stream()
                .filter(event -> event.getType().equals(eventType))
                .collect(Collectors.toList());
    }

    /**
     * Returns the events of a match that happened before or at minute
     * @param matchId ID of the match to query
     * @param minute Last valid event time
     * @return All events that happened at or before minute
     */
    public ArrayList<Event> getEventsUntil(int matchId, int minute) throws ChangeSetPersister.NotFoundException {
        var match = matchRepository.findById(matchId).orElseThrow(ChangeSetPersister.NotFoundException::new);
        return (ArrayList<Event>)
            match.getEvents()
                .stream()
                .takeWhile(event -> event.getTime() <= minute)
                .collect(Collectors.toList());
    }


    /**
     * Calculates points a player achieved during a match, and provides the player's events.
     * @param matchId ID of the match to query
     * @param playerId ID of the player to query
     * @return Pair of player points and their events
     */
    public PlayerDataResponse getPlayerInfo(int matchId, int playerId) throws ChangeSetPersister.NotFoundException {
        var match = matchRepository.findById(matchId).orElseThrow(ChangeSetPersister.NotFoundException::new);

        // Calculate players points total for the match
        var playerPoints = match.getEvents()
            .stream()
            .filter(event -> event.getPlayer() != null && event.getPlayer().getId() == playerId)
            .map(this::calculateEventScore)
            .reduce(0, Integer::sum);

        // Find all the events the player participated in
        var playerEvents = match.getEvents()
                .stream()
                .filter(event -> event.getPlayer() != null && event.getPlayer().getId() == playerId)
                .collect(Collectors.toList());

        // And finally, set player points correctly
        playerEvents.forEach(event -> {
            event.getPlayer().setPoints(playerPoints);
            playerRepository.save(event.getPlayer());
        });

        return new PlayerDataResponse(playerPoints,  playerEvents);
    }

    /**
     * Calculates scores for players of the winning team (or both if a draw) who scored and returns the players.
     * @param matchId ID of the match to query
     * @return ArrayList of Players with calculated scores
     */
    public ArrayList<Player> getScorers(int matchId) throws ChangeSetPersister.NotFoundException {
        var match = matchRepository.findById(matchId).orElseThrow(ChangeSetPersister.NotFoundException::new);

        var scores = calculatePlayerScores(match);
        var scorerIds = findScorers(match);

        // Filter scores to remove players who did not score
        scores.entrySet().removeIf(entry -> !scorerIds.contains(entry.getKey()));
        // And filter by team
        if (match.getWinner() != Side.both) {
            scores.entrySet().removeIf(entry -> !entry.getValue().getTeam().equals(match.getWinner()));
        }

        return new ArrayList<>(scores.values());
    }


    /**
     * Calculates scores of all players in the match
     */
    private HashMap<Integer, Player> calculatePlayerScores(Match match) {
        var players = new HashMap<Integer, Player>();
        for (var event : match.getEvents()){
            var eventPlayer = event.getPlayer();
            if (eventPlayer != null) {
                eventPlayer.addPoints(calculateEventScore(event));
                // Check if player was already seen, and just add up their score, or if its a new player, insert into map
                players.merge(eventPlayer.getId(),
                        eventPlayer,
                        (id, player) -> { player.addPoints(calculateEventScore(event)); return player;});
            }
        }
        return players;
    }


    /**
     * Finds ids of all players in the match that scored
     */
    private List<Integer> findScorers(Match match) {
        return match.getEvents()
                .stream()
                .filter(e -> e.getType().equals("goal"))
                .map(e -> e.getPlayer().getId())
                .collect(Collectors.toList());
    }

    private int calculateEventScore(Event event) {
        if (event.getType().equals("goal")) {
            return _settings.getGoalPoints();
        } else if (event.getType().equals("card")) {
            return _settings.getCardPoints();
        }
        return 0;
    }
}
